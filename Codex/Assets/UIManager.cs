﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour {

    public static UIManager Instance;

    public enum NotificationToDisplay
    {
        FirstSymbol,
        FirstWheel,
        Mask,
        Box,
        Dagger,
        Torch,
        Book
    }

    [Header("UI elements")]
    public IndicationTextUI importantTextUI;
    public IndicationTextUI notificationTextUI;
    public IndicationTextUI notificationTextUINoButton;
    public IndicationTextUI helperTextUI;
    public EndingPanel endingPanel;

    [Header("Important texts")]
    public string chapterCompletedText = "Chapter completed";

    [Header("Notification texts")]
    public TextAsset firstSymbol;
    public TextAsset firstWheel;
    public TextAsset first3DObject;
    public TextAsset mask;
    public TextAsset box;
    public TextAsset dagger;
    public TextAsset torch;
    public TextAsset book;

    bool firstSymbolScanned;
    bool firstWheelScanned;
    bool first3DObjectScanned;

    private void Start() {
        if (!Instance) {
            Instance = this;
        }
        else {
            Destroy(this);
        }
        endingPanel.gameObject.SetActive(false);
    }

    private void DisplayText(IndicationTextUI textUI, string text) {
        textUI.DisplayNewText(text);
    }

    private void DisplayText(IndicationTextUI textUI, TextAsset textAsset) {
        textUI.DisplayNewText(textAsset);
    }

    public void DataRetrieved(float delay) {
        StartCoroutine(WaitBeforeDisplay(delay, importantTextUI, chapterCompletedText));
    }

    public void HelperText(string helperText, GameObject go) {
        if (TextAlreadyDisplayed(go)) {
            return;
        }
        DisplayText(helperTextUI, helperText);
    }

    public void HelperText(TextAsset textAsset, GameObject go) {
        if (TextAlreadyDisplayed(go)) {
            return;
        }
        DisplayText(helperTextUI, textAsset);
    }

    public IEnumerator WaitBeforeDisplay(float delay, IndicationTextUI textUI, string text) {
        yield return new WaitForSeconds(delay);
        DisplayText(textUI, text);
    }
    
    public bool TextAlreadyDisplayed(GameObject go) {
        if (go) {
            if (go.GetComponent<InteractiveButton>() || go.GetComponent<Constellation>()) {
                if (firstSymbolScanned) {
                    return true;
                }
                else {
                    firstSymbolScanned = true;
                    return false;
                }
            }
            else if (go.GetComponent<WheelPuzzle>()) {
                if (firstWheelScanned) {
                    return true;
                }
                else {
                    firstWheelScanned = true;
                    return false;
                }
            }
        }
        return false;
    }

    public void StartEnding() {
        endingPanel.gameObject.SetActive(true);
        PostProcessManager.Instance.StartEndingEffects();
    }

    public void NotifFirstSymbol() {
        if (!firstSymbolScanned) {
            notificationTextUINoButton.displayTime = 3f;
            DisplayText(notificationTextUINoButton, firstSymbol);
            firstSymbolScanned = true;
        }
    }

    public void NotifFirstWheel() {
        if (!firstWheelScanned) {
            DisplayText(notificationTextUINoButton, firstWheel);
            firstWheelScanned = true;
        }
    }

    public void NotifMask() {
        string toDisplay = "";
        notificationTextUINoButton.displayTime = 6f;
        TryAddFirst3DObjectText(ref toDisplay);
        toDisplay += System.Text.Encoding.Default.GetString(mask.bytes);
        DisplayText(notificationTextUINoButton, toDisplay);
    }

    public void NotifBox() {
        string toDisplay = "";
        notificationTextUINoButton.displayTime = 6f;
        TryAddFirst3DObjectText(ref toDisplay);
        toDisplay += System.Text.Encoding.Default.GetString(box.bytes);
        DisplayText(notificationTextUINoButton, toDisplay);
    }

    public void NotifDagger() {
        string toDisplay = "";
        notificationTextUINoButton.displayTime = 6f;
        TryAddFirst3DObjectText(ref toDisplay);
        toDisplay += System.Text.Encoding.Default.GetString(dagger.bytes);
        DisplayText(notificationTextUINoButton, toDisplay);
    }

    public void NotifTorch() {
        string toDisplay = "";
        notificationTextUINoButton.displayTime = 6f;
        TryAddFirstSymbolText(ref toDisplay);
        TryAddFirst3DObjectText(ref toDisplay);
        toDisplay += System.Text.Encoding.Default.GetString(torch.bytes);
        DisplayText(notificationTextUINoButton, toDisplay);
    }

    public void NotifBook() {
        string toDisplay = "";
        notificationTextUINoButton.displayTime = 6f;
        TryAddFirstSymbolText(ref toDisplay);
        TryAddFirst3DObjectText(ref toDisplay);
        toDisplay += System.Text.Encoding.Default.GetString(book.bytes);
        DisplayText(notificationTextUINoButton, toDisplay);
    }

    public void TryAddFirstSymbolText(ref string toDisplay) {
        if (!firstSymbolScanned) {
            toDisplay += firstSymbol.text + '\n';
            firstSymbolScanned = true;
        }
    }

    public void TryAddFirst3DObjectText(ref string toDisplay) {
        if (!first3DObjectScanned) {
            toDisplay += first3DObject.text + '\n';
            first3DObjectScanned = true;
        }
    }

    public void NotifFindOtherSymbols(float waitTime = 0.5f) {
        notificationTextUINoButton.displayTime = 2.5f;
        StartCoroutine(WaitBeforeDisplay(waitTime, notificationTextUINoButton, "Find similar symbols to complete chapter"));
    }

    public void NotifInvalidSymbol(float waitTime = 0.3f) {
        notificationTextUINoButton.displayTime = 1f;
        StartCoroutine(WaitBeforeDisplay(waitTime, notificationTextUINoButton, "Invalid symbol"));
    }

    public void NotifCorrectSymbol(float waitTime = 0.3f) {
        notificationTextUINoButton.displayTime = 1f;
        StartCoroutine(WaitBeforeDisplay(waitTime, notificationTextUINoButton, "Correct symbol"));
    }

    public void NotifInvalidCombination(float waitTime = 0.3f) {
        notificationTextUINoButton.displayTime = 1.5f;
        StartCoroutine(WaitBeforeDisplay(waitTime, notificationTextUINoButton, "Combination invalid"));
    }

    public void NotifDataChunkRetrieved(float waitTime = 1f) {
        notificationTextUINoButton.displayTime = 2f;
        StartCoroutine(WaitBeforeDisplay(waitTime, notificationTextUINoButton, "Data retrieved"));
    }

    public void NotifPreventExtinction() {
        notificationTextUINoButton.displayTime = 2f;
        DisplayText(notificationTextUINoButton, "Device will shutdown in two minutes");
    }


}
