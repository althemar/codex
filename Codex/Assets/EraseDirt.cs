﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EraseDirt : MonoBehaviour {

    public float eraseSpeed = 1;

    public AK.Wwise.Event eraseSound;

    Renderer rendererDirty;

    float eraseProgress = 0;

    Coroutine c;

    const string alphaCutoffVariable = "Vector1_3C641685";

    public void Start() {
        rendererDirty = GetComponent<Renderer>();
        eraseSpeed = 1 / eraseSpeed;
        eraseProgress = rendererDirty.material.GetFloat(alphaCutoffVariable);
    }

    public void StartErase() {
        c = StartCoroutine(Erase());
        SoundManager.PlayEvent(eraseSound);
    }

    public IEnumerator Erase() {
        while (eraseProgress <= 1) {
            eraseProgress += eraseSpeed * Time.deltaTime;
            rendererDirty.material.SetFloat(alphaCutoffVariable, eraseProgress);
            yield return null;
        }
    }
}
