﻿using UnityEngine;
using UnityEditor;


[CustomEditor(typeof(Chapter))]
public class ChapterEditor : Editor
{
    SerializedProperty lockables;

    private void OnEnable() {
        lockables = serializedObject.FindProperty("lockableObjects");
    }

    public override void OnInspectorGUI() {

        DrawDefaultInspector();

        EditorGUILayout.Space();

        EditorGUILayout.LabelField("Lockable objects", EditorStyles.boldLabel);

        serializedObject.Update(); 

        DrawLockables();

        DrawAddLockableButton();
        
        serializedObject.ApplyModifiedProperties();
    }

    // Draw the lockables object of the chapter
    private void DrawLockables() {


        for (int i = 0; i < lockables.arraySize; i++) {

            EditorGUILayout.BeginHorizontal();

            SerializedProperty lockable = lockables.GetArrayElementAtIndex(i);


            // Lockable field
            if (lockable.objectReferenceValue) {
                EditorGUILayout.LabelField(lockable.objectReferenceValue.ToString().Split('(')[0]);
            }
            else {
                EditorGUI.BeginChangeCheck();
                EditorGUILayout.PropertyField(lockable, GUIContent.none);
                if (EditorGUI.EndChangeCheck()) {
                    if (EditorHelper.OccurencesOfObjectInArray(lockables, lockable.objectReferenceInstanceIDValue) == 2) {
                        lockables.GetArrayElementAtIndex(i).objectReferenceValue = null;
                    }
                }
            }

            // Lockable reordering buttons
            if (i == 0) {
                GUI.enabled = false;
            }
            if (GUILayout.Button("Up")) {
                EditorHelper.SwitchElementsInArray(lockables, i, i - 1);
            }
            GUI.enabled = true;

            if (i == lockables.arraySize - 1) {
                GUI.enabled = false;
            }
            if (GUILayout.Button("Down")) {
                EditorHelper.SwitchElementsInArray(lockables, i, i + 1);
            }
            GUI.enabled = true;

            // Lockable deletion button
            if (GUILayout.Button("-")) {
                if (lockable.objectReferenceValue) {
                    SerializedObject lockableObjects = new SerializedObject(lockable.objectReferenceValue);
                    SerializedProperty objectsToUnlock = lockableObjects.FindProperty("objectsToUnlock");
                    for (int j = 0; j < objectsToUnlock.arraySize; j++) {
                        RemoveFromObjectsNeeded(objectsToUnlock.GetArrayElementAtIndex(j).objectReferenceValue, lockable.objectReferenceValue);
                        EditorHelper.DeleteElementAtIndex(objectsToUnlock, i);
                    }
                    lockableObjects.ApplyModifiedProperties();
                }
                EditorHelper.DeleteElementAtIndex(lockables, i);
                EditorGUILayout.EndHorizontal();
                return;
            }
            EditorGUILayout.EndHorizontal();

            if (lockable.objectReferenceValue) {
                SerializedObject lockableObjects = new SerializedObject(lockable.objectReferenceValue);
                SerializedProperty objectsToUnlock = lockableObjects.FindProperty("objectsToUnlock");
                DrawObjectsToUnlock(lockable, objectsToUnlock);
                lockableObjects.ApplyModifiedProperties();
            }

            if (i != lockables.arraySize - 1) {
                EditorGUILayout.Space();
                EditorGUILayout.LabelField("", GUI.skin.horizontalSlider);
            }
            EditorGUILayout.Space();
        }
    }

    // Draw the objects that the object unlock
    public void DrawObjectsToUnlock(SerializedProperty lockable, SerializedProperty objectsToUnlock) {

        EditorGUI.indentLevel++;

        EditorGUILayout.BeginHorizontal();
        EditorGUILayout.LabelField("Objects to unlock");

        
        if (EditorHelper.HasNullElementInArray(objectsToUnlock)) {
            GUI.enabled = false;
        }
        if (GUILayout.Button("Add object to unlock")) {
            EditorHelper.AddNullElementToArray(objectsToUnlock);
        }
        GUI.enabled = true;

        EditorGUILayout.EndHorizontal();

        for (int i = 0; i < objectsToUnlock.arraySize; i++) {
            DrawObjectToUnlockField(lockable, objectsToUnlock, i);
        }

        EditorGUI.indentLevel--;
    }

    // Draw a button that add a lockable to the chapter
    private void DrawAddLockableButton() {
        if (EditorHelper.HasNullElementInArray(lockables)) {
            GUI.enabled = false;
        }
        if (GUILayout.Button("Add lockable")) {
            EditorHelper.AddNullElementToArray(lockables);
        }
        GUI.enabled = true;
    }

    // Draw an object to unlock
    private void DrawObjectToUnlockField(SerializedProperty lockable, SerializedProperty objectsToUnlock, int i) {
        EditorGUILayout.BeginHorizontal();

        SerializedProperty objectToUnlock = objectsToUnlock.GetArrayElementAtIndex(i);

        Object previousObject = objectToUnlock.objectReferenceValue;

        EditorGUI.BeginChangeCheck();
        EditorGUILayout.PropertyField(objectToUnlock, GUIContent.none);
        if (EditorGUI.EndChangeCheck()) {
            if (EditorHelper.OccurencesOfObjectInArray(objectsToUnlock, objectToUnlock.objectReferenceInstanceIDValue) == 2
                || lockable.objectReferenceInstanceIDValue == objectToUnlock.objectReferenceInstanceIDValue) {

                objectsToUnlock.GetArrayElementAtIndex(i).objectReferenceValue = null;
            }
            else if (objectToUnlock.objectReferenceValue != previousObject) {
                if (previousObject != null) {
                    RemoveFromObjectsNeeded(previousObject, lockable.objectReferenceValue);
                }
                SerializedObject newObjectToUnlock = new SerializedObject(objectToUnlock.objectReferenceValue);
                SerializedProperty newObjectsNeeded = newObjectToUnlock.FindProperty("objectsNeeded");
                EditorHelper.AddElementToArray(newObjectsNeeded, lockable.objectReferenceValue);
                if (newObjectsNeeded.arraySize > 0) {
                    SerializedProperty locked = newObjectToUnlock.FindProperty("locked");
                    locked.boolValue = true;
                }
                newObjectToUnlock.ApplyModifiedProperties();
            }
        }

        if (GUILayout.Button("-")) {
            if (objectToUnlock.objectReferenceValue) {
                RemoveFromObjectsNeeded(objectToUnlock.objectReferenceValue, lockable.objectReferenceValue);
            }
            EditorHelper.DeleteElementAtIndex(objectsToUnlock, i);
        }

        EditorGUILayout.EndHorizontal();
    }

    // Remove an element from the objects needed
    private void RemoveFromObjectsNeeded(Object objectReference, Object objectToRemove) {
        SerializedObject objectToUnlockSerialized = new SerializedObject(objectReference);
        SerializedProperty objectsNeeded = objectToUnlockSerialized.FindProperty("objectsNeeded");
        EditorHelper.DeleteObjectInArray(objectsNeeded, objectToRemove.GetInstanceID());
        if (objectsNeeded.arraySize == 0) {
            SerializedProperty locked = objectToUnlockSerialized.FindProperty("locked");
            locked.boolValue = false;
        }
        objectToUnlockSerialized.ApplyModifiedProperties();
    }

    
}
