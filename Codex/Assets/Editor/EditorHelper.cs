﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

/*
 * EditorHelper
 * Methods useful for editor scripting with serialized property.
 */

public class EditorHelper {

    // Return true if the array has a null element 
    public static bool HasNullElementInArray(SerializedProperty property) {
        if (property.arraySize == 0) {
            return false;
        }
        for (int i = 0; i < property.arraySize; i++) {
            if (!property.GetArrayElementAtIndex(i).objectReferenceValue) {
                return true;
            }
        }
        return false;
    }

    // Add an element at the end of the array
    public static void AddElementToArray(SerializedProperty property, Object element) {
        AddNullElementToArray(property);
        property.GetArrayElementAtIndex(property.arraySize - 1).objectReferenceValue = element;
    }

    // Add a null element at the end of the array
    public static void AddNullElementToArray(SerializedProperty property) {
        property.InsertArrayElementAtIndex(property.arraySize);
        if (property.GetArrayElementAtIndex(property.arraySize - 1).objectReferenceValue) {
            property.DeleteArrayElementAtIndex(property.arraySize - 1);
        }
    }

    // Delete an element in the array with its index
    public static void DeleteElementAtIndex(SerializedProperty property, int index) {
        if (property.GetArrayElementAtIndex(index).objectReferenceValue) {
            property.DeleteArrayElementAtIndex(index);
        }
        property.DeleteArrayElementAtIndex(index);
    }

    // Delete an object in the array with its reference
    public static void DeleteObjectInArray(SerializedProperty property, int objectReferenceInstanceIDValue) {
        for (int i = 0; i < property.arraySize; i++) {
            if (property.GetArrayElementAtIndex(i).objectReferenceInstanceIDValue == objectReferenceInstanceIDValue) {
                DeleteElementAtIndex(property, i);
                break;
            }
        }
    }

    // Switch two elements in the array
    public static void SwitchElementsInArray(SerializedProperty property, int index1, int index2) {
        SerializedProperty element1 = property.GetArrayElementAtIndex(index1);
        SerializedProperty element2 = property.GetArrayElementAtIndex(index2);

        Object tmpObject = element1.objectReferenceValue;
        element1.objectReferenceValue = element2.objectReferenceValue;
        element2.objectReferenceValue = tmpObject;
    }

    // Return true if an object is in an array
    public static bool ObjectInArray(SerializedProperty property, int objectReferenceInstanceIDValue) {
        for (int i = 0; i < property.arraySize; i++) {
            if (property.GetArrayElementAtIndex(i).objectReferenceInstanceIDValue == objectReferenceInstanceIDValue) {
                return true;
            }
        }
        return false;
    }

    // Return the number of occurences of an object in the array
    public static int OccurencesOfObjectInArray(SerializedProperty property, int objectReferenceInstanceIDValue) {
        int count = 0;
        for (int i = 0; i < property.arraySize; i++) {
            if (property.GetArrayElementAtIndex(i).objectReferenceInstanceIDValue == objectReferenceInstanceIDValue) {
                count++;
            }
        }
        return count;
    }
}
