﻿using System.Collections.Generic;
using UnityEngine;

/*
 * Chapter
 * A chapter contains multiple objects to augment. It has a beginning and an end.
 */

[System.Serializable]
public class Chapter : MonoBehaviour
{
    /*
     * Members
     */

    [SerializeField]
    ChapterState chapterState = ChapterState.CanBeOpened;   // The state of the chapter.

    [SerializeField]
    public LockableAugmented chapterBeginning;              // The first object of the chapter. Validate it set the state to Opened. To see it, the state must be CanBeOpened.

    [SerializeField]
    public LockableAugmented chapterEnding;                 // The last object of the chapter. Validate it set the state to Finished.

    [SerializeField]
    [HideInInspector]
    List<LockableAugmented> lockableObjects = new List<LockableAugmented>();    // The list of lockable objects in the chapter. Describe the progression of the chapter

    [Header("Wwise")]
    public AK.Wwise.State enigmaMusic;


    /*
     * Properties
     */

    public List<LockableAugmented> LockableObjects
    {
        get { return lockableObjects; }
    }

    public ChapterState ChapterState
    {
        get { return chapterState; }
    }

    public LockableAugmented ChapterBeginning
    {
        get { return chapterBeginning; }
    }

    public LockableAugmented ChapterEnding
    {
        get { return chapterEnding; }
    }

    /*
     * Methods
     */

    private void Awake() {
        AugmentedObject[] augmentedObjects = GetComponentsInChildren<AugmentedObject>(true);
        for (int i = 0; i < augmentedObjects.Length; i++) {
            augmentedObjects[i].Chapter = this;
        }
    }

    // Set the state to Opened.
    public void OpenChapter() {
        chapterState = ChapterState.Opened;
    }

    // Add a null reference at the ends of lockableObjects.
    public void AddEmptyLockable() {
        lockableObjects.Add(null);
    }

    // Set a lockable objects.
    public bool SetLockable(int index, LockableAugmented lockable) {
        if (IsAlreadyInList(lockable)) {
            return false;
        }
        lockableObjects[index] = lockable;

        if (lockableObjects.Count == 1) {
            lockable.AddEmptyObjectToUnlock();
        }
        return true;
    }

    // Remove a lockable object.
    public void RemoveLockable(int index) {
        if (lockableObjects[index]) {
            lockableObjects[index].ClearObjectsToUnlock();
        }
        lockableObjects.RemoveAt(index);
    }

    // Return true if there is a null lockable.
    public bool HasNullLockable() {
        return lockableObjects.Contains(null);
    }

    // Return true if a lockable is already in the list.
    public bool IsAlreadyInList(LockableAugmented lockable) {
        return lockableObjects.Contains(lockable);
    }

    // Switch two lockables in the list.
    public void SwitchLockables(int index1, int index2) {
        LockableAugmented tmp = LockableObjects[index1];
        LockableObjects[index1] = LockableObjects[index2];
        LockableObjects[index2] = tmp;
    }
}