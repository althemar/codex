﻿using UnityEngine;
using System.Collections.Generic;
/*
 * Ring
 * A ring is a part of a wheel puzzle. It must be aligned to its original rotation to solve the puzzle
 */

public class Ring : MonoBehaviour
{
    /*
     * Members
     */

    public float turnSpeed = 15f;

    WheelPuzzle wheelPuzzle;    // The wheel puzzle associated

    // Rotation members
    bool selected;              // If the ring is selected. Must be true to allow rotation
    Vector3 beginRot;           // The previous rotation of the ring when it is rotating
    float originalRotation;     // The original rotation of the ring

    float originalRotationX;
    float originalRotationY;
    float originalRotationZ;

    // Snapping members
    bool align;                     // If the ring must snap. Set to true when the player release it
    Quaternion alignBeginRotation;  // The begin rotation of the snap
    float alignRotation;            // The end rotation of the snap
    float progress;                 // The progress of the snap

    const string selectionSound = "Play_Paper_Rune_Selection";
    const string releasedSound = "Play_Paper_Rune_Movement";

    

    /*
     * Properties
     */

    public float OriginalRotation
    {
        get { return originalRotation; }
    }

    public bool Selected
    {
        get { return selected; }
        set { selected = value; }
    }

    /*
     * Methods
     */

    public void Awake()
    {
        originalRotation = transform.localEulerAngles.y;
        originalRotationX = transform.localEulerAngles.x;
        originalRotationY = transform.localEulerAngles.y;
        originalRotationZ = transform.localEulerAngles.z;

        wheelPuzzle = GetComponentInParent<WheelPuzzle>();
        align = false;
        turnSpeed = 1 / turnSpeed;
    }

    public void Update()
    {
        // Rotate to nearest 
        if (align)
        {
            if (progress <= 1f)
            {
                transform.localRotation = Quaternion.Slerp(alignBeginRotation, Quaternion.Euler(originalRotationX, alignRotation, originalRotationZ), progress);
                progress += 0.08f;
            }
            else
            {
                align = false;
                wheelPuzzle.CheckIfSolved();
                return;
            }
        }
    }


    // Begin the rotation of the ring. Called when touch.began
    public void BeginRotate(Vector3 hitPos)
    {
        if (wheelPuzzle.WheelState != WheelPuzzleState.Unlocked )
        {
            return;
        }
        selected = true;
        align = false;
        beginRot = hitPos - transform.position;

        SoundManager.PlayEvent(wheelPuzzle.ringSelected[wheelPuzzle.GetRingIndex(this)]);
    }

    // Rotate the ring. It follows the finger position of it. Called when touch.moved
    public void Rotate(Vector3 hitPos) {
        if (selected && wheelPuzzle.WheelState == WheelPuzzleState.Unlocked && wheelPuzzle.Augmented.BurningComplete) {
            Vector3 endRot = hitPos - transform.position;
            endRot.z = 0;
            float angle = Vector3.SignedAngle(beginRot, endRot, new Vector3(originalRotationX, originalRotationY, 1));
            transform.Rotate(new Vector3(0, 0, -angle));
            beginRot = endRot;
        }
    }

    // End the rotation of the ring. Called when touch.ended. 
    // Set the snap rotation.
    public void EndRotate()
    {
        if (!selected) {
            return;
        }

        align = true;
        selected = false;

        float endRot = transform.localEulerAngles.y;
        float localEulerAnglesY = transform.localEulerAngles.y;
        if (localEulerAnglesY > originalRotation) {
            //endRot = localEulerAnglesY - originalRotation;
        }
        else if (localEulerAnglesY > 360){
            endRot = localEulerAnglesY + originalRotation * 2;
            while (endRot > 360) {
                endRot -= 360;
            }
        }

        for (int i = 0; i < wheelPuzzle.AnglesList.Count; i++)
        {
            if (endRot <= wheelPuzzle.AnglesList[i])
            {
                alignRotation = wheelPuzzle.AnglesList[i] - wheelPuzzle.SymbolSize / 2;
                break;
            }
            else if (i == wheelPuzzle.AnglesList.Count - 1)
            {
                alignRotation = 360;
                break;
            }
        }
        progress = 0;
        alignBeginRotation = transform.localRotation;

        SoundManager.PlayEvent(wheelPuzzle.ringReleased);
    }
}

