﻿using UnityEngine;

/*
 * RotateObject
 * Allow to rotate an object freely on all axis.
 */

public class RotateObject : MonoBehaviour {

    /*
     * Members
     */

    public bool rotateAroundX = true;
    public bool rotateAroundY = true;
    public bool rotateParent;           // If the rotation must be applied to the object or to its parent.
    public float rotationSpeed = 1f;    // Modify the speed of the rotation. Higher is slower.
    public bool cameraView;
    public Space space;

    Vector2 lastScreenPosition;          // The last input position on the screen.
    Camera cam;                          // The camera used to rotate

    Transform rotateAxis;
    Transform objectToRotate;

    /*
     * Methods
     */
  
    private void Start() {
        cam = Camera.main;
        if (cameraView) {
            rotateAxis = cam.transform;
        }
        else {
            rotateAxis = transform;
        }
        if (rotateParent) {
            objectToRotate = transform.parent;
        }
        else {
            objectToRotate = transform;
        }
    }

    public void BeginRotate(Vector3 hitPos) {
        lastScreenPosition = hitPos;
    }

    public void Rotate(Vector3 hitPos) {
        float f_difX = (lastScreenPosition.x - hitPos.x) / rotationSpeed;
        float f_difY = -(lastScreenPosition.y - hitPos.y) / rotationSpeed ;

        if (rotateAroundX) {
            objectToRotate.Rotate(rotateAxis.up, f_difX, space);
        }
        if (rotateAroundY) {
            objectToRotate.Rotate(rotateAxis.right, f_difY, space);
        }
        lastScreenPosition = hitPos;
    }
}
