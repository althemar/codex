﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class HandHelper : MonoBehaviour
{

    public Sprite dragToObject;
    public Sprite returnToPosition;

    public Transform originalPosition;

    Vector2 objectPosition;

    bool activated;
    bool toObject;

    Image image;

    public float speed;

    float progress;

    Vector2 target;


    public bool Activated
    {
        get { return activated; }
    }
    

    public void Start() {
        objectPosition = new Vector2(Screen.width / 2, Screen.height / 2);
        image = GetComponent<Image>();
        image.enabled = false;
        speed = 1 / speed;
    }



    public void LaunchAnimation() {
        activated = true;
        toObject = true;
        transform.position = originalPosition.position;
        image.enabled = true;
        progress = 0;
        target = objectPosition;
    }

    public void StopHelper() {
        activated = false;
        image.enabled = false;
    }

    private void Update() {
        if (activated) {
            progress += speed * Time.deltaTime;
            transform.position = Vector3.Lerp(transform.position, target, progress);

            if (progress >= 0.3) {
                toObject = !toObject;
                progress = 0;
                if (toObject) {
                    image.sprite = dragToObject;
                    target = objectPosition;
                }
                else {
                    image.sprite = returnToPosition;
                    target = originalPosition.position;
                }
            }
        }
    }

    private IEnumerator HideHandHelper() {
        while (progress <= 1) {
            Color tmpColor = image.color;
            tmpColor.a = Mathf.Lerp(image.color.a, 0, progress);
            image.color = tmpColor;
            progress += speed * Time.deltaTime;
            yield return null;
        }
    }
}
