﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/*
 * LockableAugmented
 * A lockable augmented is an object that appear when scanning. It can invisible if required actions are not done. 
 */

[System.Serializable]
public class LockableAugmented : AugmentedObject {

    /*
     * Members
     */

    [SerializeField]
    [HideInInspector]
    bool locked;                             // If the object is not visible

    [SerializeField]
    //[HideInInspector]
    List<LockableAugmented> objectsNeeded = new List<LockableAugmented>();   // The list of objects that must be validated to unlock the current object

    [SerializeField]
    [HideInInspector]
    List<LockableAugmented> objectsToUnlock = new List<LockableAugmented>(); // The list of objects that the current object unlocks when it is validated

    public UnityEvent onValidate;

    bool validated = false;     // If the current object is validated


    /*
     * Properties
     */

    public bool Locked
    {
        get { return locked; }
    }

    public bool Validated
    {
        get { return validated; }
    }

    public List<LockableAugmented> ObjectsNeeded
    {
        get { return objectsNeeded; }
    }

    public List<LockableAugmented> ObjectsToUnlock
    {
        get { return objectsToUnlock; }
    }

    /*
     * Methods
     */

    // Check if all the required objects are validated
    private void CheckIfCanBeUnlocked()
    {
        foreach (LockableAugmented lockable in objectsNeeded)
        {
            if (!lockable.Validated)
            {
                return;
            }
        }

        locked = false;
        if (TargetIsTracked) {
            ActivateAugmented(true);
        }
    }

    // Validate and object and check if the next objects can be unlocked
    public void Validate()
    {
        if (validated)
        {
            return;
        }
        validated = true;
        onValidate.Invoke();

        if (this == Chapter.chapterBeginning) {
            Chapter.OpenChapter();
        }
        for (int i = 0; i < objectsToUnlock.Count; i++)
        {
            objectsToUnlock[i].CheckIfCanBeUnlocked();
        }
    }

    public void ValidateWithDelay(float delay) {
        StartCoroutine(WaitBeforeValidate(delay));
    }

    public IEnumerator WaitBeforeValidate(float delay) {
        yield return new WaitForSeconds(delay);
        Validate();
    }

    public void ClearObjectsToUnlock() {
        for (int i = 0; i < objectsToUnlock.Count; i++) {
            if (objectsToUnlock[i]) {
                objectsToUnlock[i].RemoveNeededObject(this);
            }
        }
        objectsToUnlock.Clear();
    }

    public bool SetObjectToUnlock(int index, LockableAugmented lockable) {
        if (this == lockable || objectsToUnlock.Contains(lockable)) {
            return false;
        }
        if (objectsToUnlock[index]) {
            ObjectsToUnlock[index].RemoveNeededObject(this);
        }
        objectsToUnlock[index] = lockable;
        lockable.AddNeededObject(this);
        return true;
    }

    public bool AddEmptyObjectToUnlock() {
        if (objectsToUnlock.Contains(null)) {
            return false;
        }
        objectsToUnlock.Add(null);
        return true;
    }

    public void RemoveObjectToUnlock(int index) {
        if (objectsToUnlock[index]) {
            objectsToUnlock[index].RemoveNeededObject(this);
        }
        objectsToUnlock.RemoveAt(index);
    }

    public void RemoveNeededObject(LockableAugmented lockable) {
        objectsNeeded.Remove(lockable);
        if (objectsNeeded.Count == 0) {
            locked = false;
        }
    }

    public bool HasNullObjectToUnlock() {
        return objectsToUnlock.Contains(null);
    }

    public void AddNeededObject(LockableAugmented lockable) {
        objectsNeeded.Add(lockable);
        locked = true;
    }

    


}
