﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractiveButton : MonoBehaviour {

    bool validated = false;
    public bool symbolIsRight;

    public ParticleSystem neutralPS;
    public ParticleSystem activatedPS;

    public Color neutralColor;
    public Color rightSymbolColor;
    public Color wrongSymbolColor;

    public float speedColorChange = 2f;

    bool colorChanging;
    float progressColor;
    Color beginColor;
    Color targetColor;

    const string textHelpPath = "Texts/symbolHelp";


    public bool Validated
    {
        get { return validated; }
        set { validated = true; }
    }
    

    private void Start() {
        speedColorChange = 1f / speedColorChange;
    }

    private void Update() {
        if (colorChanging) {
            progressColor += speedColorChange * Time.deltaTime;
            Color currentColor = Color.Lerp(beginColor, targetColor, progressColor);
            var neutralMain = neutralPS.main;
            neutralMain.startColor = currentColor;

            if (progressColor >= 1) {
                colorChanging = false;
            }
        }
    }

    public void TryUnlock() {

        if (!validated && symbolIsRight) {
            GetComponent<MeshRenderer>().enabled = false;
            ActivateParticleSystem(rightSymbolColor, rightSymbolColor);
        }
        else if (!symbolIsRight) {
            ActivateParticleSystem(wrongSymbolColor, wrongSymbolColor);
        }
    }

    public void OnTrack() {

    }

    public void Unvalidate() {
        if (validated) {
            validated = false;
            ActivateParticleSystem(neutralColor, wrongSymbolColor);
        }
    }

    public void ActivateParticleSystem(Color neutral, Color explosion) {
        progressColor = 0;
        colorChanging = true;
        var neutralMain = neutralPS.main;
        beginColor = neutralMain.startColor.color;
        targetColor = neutral;
        var activateMain = activatedPS.main;
        activateMain.startColor = explosion;
        activatedPS.Play();
    }
}
