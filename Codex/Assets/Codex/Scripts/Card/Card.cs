﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

/*
 * Card
 * A card is gathered when a rune is activated. It can be used on a wheel to activate it. 
 */

// TODO Transform the pointers events to checks in the update (may improve performances).

public class Card : MonoBehaviour, IPointerDownHandler, IDragHandler
{
    // The states of the card
    enum CardState
    {
        Spawning,
        ReturningToPosition,
        SelectionBegin,
        Validating,
        DoNothing,
    }
    


    /*
     * Members 
     */

    public float selectedSize;          // Size of the card when it is selected.
    public float selectedSizeSpeed;     // Size increase speed when the card selection begin.

    public float PositionResetSpeed;    // Speed of the card when the card is released and it is returning to the original position.

    public float validationSize;        // Size of the card when it is used right.
    public float validationSizeSpeed;   // Size increase speed when the card is used right.

    LockableAugmented objectToUnlock;        // The wheel to unlock with the card.

    Image image;                        // The image UI of the card.
    RectTransform rectTransform;        // The rectTransform of the card.
    CardSO cardSO;                      // The card scriptable object with the associated properties.

    Vector3 differenceBetweenCenterClick;   // The difference between the center of the card and the click position.

    Vector3 originalPosition;       // The original position of the card.

    Vector3 selectedSizeVector;     // Vector for the selected size. Initialized in Awake().
    Vector3 validationSizeVector;   // Vector for the validation size. Initialized in Awake().


    float progress;

    CardState state;
    CardState previousState;

    bool activateHelper;

    /*
     * Properties
     */

    public CardSO CardSO
    {
        get { return cardSO; }
        set {
            cardSO = value;
            image.sprite = cardSO.image;
        }
    }

    public LockableAugmented ObjectToUnlock
    {
        get { return objectToUnlock; }
        set { objectToUnlock = value; }
    }

    public bool ActivateHelper
    {
        get { return activateHelper; }
        set { activateHelper = value; }
    }

    /*
     * Methods
     */

    private void Awake() {
        image = GetComponent<Image>();
        image.color = new Vector4(image.color.r, image.color.g, image.color.b, 0);
        rectTransform = GetComponent<RectTransform>();
        originalPosition = Inventory.Instance.CardInitPosition.transform.position;
        selectedSizeVector = new Vector3(selectedSize, selectedSize, 1);
        validationSizeVector = new Vector3(validationSize, validationSize, 1);

        selectedSizeSpeed = 1.0f / selectedSizeSpeed;
        PositionResetSpeed = 1.0f / PositionResetSpeed;
        validationSizeSpeed = 1.0f / validationSizeSpeed;
    }


    private void SwitchState(CardState newState) {
        previousState = state;
        state = newState;
    }

    // Update : Set the size and position of the card depending of its state.
    private void Update() {
        if (state == CardState.Spawning && progress <= 1) {
            transform.localScale = Vector3.Lerp(transform.localScale, selectedSizeVector, progress);
            image.color = new Vector4(image.color.r, image.color.g, image.color.b, Mathf.Lerp(image.color.a, 1, progress));
            progress +=( (1f / 4f) * Time.deltaTime);
            if (progress >= 0.7f) {
                progress = 0;
                SwitchState(CardState.ReturningToPosition);
                SoundManager.Instance.PlaySound("Play_Card_Drag_Inventory");
            }
        }
        else if (state == CardState.ReturningToPosition && progress <= 1) {
            transform.position = Vector3.Lerp(transform.position, originalPosition, progress);
            transform.localScale = Vector3.Lerp(transform.localScale, Vector3.one, progress);
            progress += PositionResetSpeed * Time.deltaTime;
            if (progress > 1) {
                progress = 0;
                if (activateHelper && !Inventory.Instance.helper.Activated) { 
                    Inventory.Instance.LaunchHelper();
                }
                SwitchState(CardState.DoNothing);
            }
        }
        else if (state == CardState.SelectionBegin && progress <= 1) {

            transform.localScale = Vector3.Lerp(transform.localScale, selectedSizeVector, progress);
            progress += selectedSizeSpeed * Time.deltaTime;
            if (progress > 1) {
                progress = 0;
                SwitchState(CardState.DoNothing);
            }
        }
        else if (state == CardState.Validating && progress <= 1) {

            transform.localScale = Vector3.Lerp(transform.localScale, validationSizeVector, progress);
            Color tmpColor = image.color;
            tmpColor.a = Mathf.Lerp(image.color.a, 0, progress);
            image.color = tmpColor;
            progress += validationSizeSpeed * Time.deltaTime;
            if (progress >= 1) {
                Destroy(gameObject);
            }
        }
    }

    // Instantiate a new card and associate the scriptable object.
    public static Card CreateCard(Card cardPrefab, CardSO _cardSO, Vector2 spawnPosition) {
        Card newCard = Instantiate(cardPrefab, Inventory.Instance.CardInitPosition.transform.position, Quaternion.identity, Inventory.Instance.transform) as Card;
        newCard.transform.SetSiblingIndex(0);
        newCard.CardSO = _cardSO;
        newCard.transform.position = spawnPosition;
        newCard.transform.localScale = Vector3.zero;
        newCard.progress = 0;
        newCard.SwitchState(CardState.Spawning);

        SoundManager.Instance.PlaySound("Play_Card_Appear");
        return newCard;
    }

    // Card touched.
    public void OnPointerDown(PointerEventData eventData) {
        if (state == CardState.Validating) {
            return;
        }
        InputHandler.Instance.SelectedCard = this;
        differenceBetweenCenterClick = rectTransform.position - new Vector3(eventData.position.x, eventData.position.y, 0);
        progress = 0;
        SwitchState(CardState.SelectionBegin);

        SoundManager.Instance.PlaySound("Play_Card_Selection");
    }

    // Drag the card.
    public void OnDrag(PointerEventData eventData) {
        rectTransform.position = new Vector3(eventData.position.x, eventData.position.y, 0) + differenceBetweenCenterClick;
    }

    // Card released.
    public void ReleaseCard(GameObject gObject) {
        InputHandler.Instance.SelectedCard = null;

        if (gObject) {
            CardReceptor cardReceptor = gObject.transform.GetComponent<CardReceptor>();

            bool isOnRightCardReceptor = (cardReceptor && cardReceptor.objectToUnlock == objectToUnlock);

            
            if (isOnRightCardReceptor) {
                cardReceptor.Unlock();
                progress = 0;
                Inventory.Instance.helper.StopHelper();
                SwitchState(CardState.Validating);
                SoundManager.Instance.PlaySound("Play_Card_Rune");
            }
            else {
                progress = 0;
                SwitchState(CardState.ReturningToPosition);
            }
        }
        else {
            progress = 0;
            SwitchState(CardState.ReturningToPosition);
        }
    }
}