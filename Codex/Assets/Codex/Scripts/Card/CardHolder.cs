﻿using UnityEngine;

/* CardHolder
 * Contains the card that will be added to the inventory.
 */

public class CardHolder : MonoBehaviour {

    public CardSO cardSO;               // The card contained.
    public LockableAugmented objectToUnlock;   // The wheel puzzle that the card unlock.
    public bool activateHelper;
    

    // Change the card of the inventory.
    public void ChangeCard() {
        Card card = Inventory.Instance.CreateCard(cardSO, Camera.main.WorldToScreenPoint(transform.position));
        card.ObjectToUnlock = objectToUnlock;
        card.ActivateHelper = activateHelper;
    }
}
