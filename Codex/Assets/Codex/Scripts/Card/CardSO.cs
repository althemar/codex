﻿using UnityEngine;

/*
 * CardSO
 * ScriptableObject that describe a card.
 */

[CreateAssetMenu(fileName = "Card", menuName = "Codex/Card")]
public class CardSO: ScriptableObject {

    public string cardName;     // name of the card.
    public Sprite image;        // Sprite of the card.
}
