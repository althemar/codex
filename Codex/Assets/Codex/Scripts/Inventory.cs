﻿using UnityEngine;

public class Inventory : MonoBehaviour {

    public Card cardPrefab;
    public GameObject CardInitPosition;

    public static Inventory Instance;

    public HandHelper helper;

    Card currentCard;

	void Awake () {
		if (Instance == null) {
            Instance = this;
        }
        else {
            Destroy(this);
        }
	}

    public Card CreateCard(CardSO cardSO, Vector2 spawnPosition) {
        if (currentCard != null) {
            Destroy(currentCard.gameObject);
        }
        currentCard = Card.CreateCard(cardPrefab, cardSO, spawnPosition);
        return currentCard;
    }

    public void LaunchHelper() {
        helper.LaunchAnimation();
    }
    
}
