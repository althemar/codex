﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class RadialProgressBar : MonoBehaviour {

    public static RadialProgressBar Instance;

    public Image progressBar;
    public float resetSpeed;
    public float fillSpeed;

    float currentAmount;
    bool filling;
    bool resetting;

    Action functionToCallWhenComplete;


    public void Awake() {
        if (Instance == null) {
            Instance = this;
        }
        else {
            Destroy(gameObject);
        }
    }

    private void Start() {
        resetSpeed = 1 / resetSpeed;
        fillSpeed = 1 / fillSpeed;
    }

    public void StartProgressBarFromBegin(Action functionToCall) {
        resetting = false;
        currentAmount = 0;
        StartProgressBar(functionToCall);
    }

    public void StartProgressBar(Action functionToCall) {
        filling = true;
        functionToCallWhenComplete = functionToCall;
        if (resetting) {
            resetting = false;
        }
        else {
            currentAmount = 0;
            progressBar.fillAmount = 0;
        }
    }

    private void Update() {
        if (filling || resetting) {
            float speed = 0;
            if (filling) {
                speed = fillSpeed;
            }
            else if (resetting) {
                speed = -resetSpeed;
            }
            currentAmount += speed * Time.deltaTime;

            progressBar.fillAmount = currentAmount;

            if (filling && currentAmount >= 1) {
                filling = false;
                currentAmount = 0;
                progressBar.fillAmount = 0;
                functionToCallWhenComplete();
            }
            else if (resetting && currentAmount <= 0) {
                resetting = false;
            }
        }
    }


    public void ResetProgress() {
        filling = false;
        resetting = true;
    }

}
