﻿using System.Collections;
using UnityEngine;
using TMPro;
using System.Text;

public class DisplayTextLetterByLetter : MonoBehaviour {
    
    public float displayBaseSpeed = 0.05f;
    public float eraseBaseSpeed = 0.05f;

    public AK.Wwise.Event playTelemetry;
    public AK.Wwise.Event stopTelemetry;
    public AK.Wwise.Event muteTelemetry;
    public AK.Wwise.Event unmuteTelemetry;

    TextMeshProUGUI text;

    string displayText;

    Coroutine c;

    int ellipsisCount = 0;

    bool allTextDisplayed;    

    public delegate void EndingDisplay();
    public event EndingDisplay OnEndingDisplay;

    public bool AllTextDisplayed
    {
        get { return allTextDisplayed; }
    }

    void Awake () {
        text = GetComponent<TextMeshProUGUI>();
	}

	public void StartDisplayText(string displayText) {
        this.displayText = displayText;
        EraseText();
        c = StartCoroutine("DisplayText");
        allTextDisplayed = false;
    }

    public void StopDisplayText() {
        if (c != null) {
            StopCoroutine(c);
        }
    }

    public void EraseText() {
        StopDisplayText();
        if (!text) {
            text = GetComponent<TextMeshProUGUI>();
        }
        text.text = "";
    }

    public void EraseTextLetterByLetter() {
        StartCoroutine(EraseTextFromBegin());
    }

    public void DisplayAllText() {
        text.text = displayText;
        StopCoroutine(c);
        allTextDisplayed = true;
    }

    public IEnumerator DisplayText() {
        int i = 0;

        SoundManager.PlayEvent(playTelemetry);

        while (i <= displayText.Length) {
            if (ellipsisCount == 0) {
                if (i > displayText.Length) {
                    allTextDisplayed = true;
                    break;
                }
                AddLetter(displayText[i++]);
                if (i >= displayText.Length) {
                    allTextDisplayed = true;
                    break;
                }
                bool continueAddLetter = true;
                while (continueAddLetter) {
                    if (i >= displayText.Length - 1) {
                        allTextDisplayed = true;
                        break;
                    }
                    char newChar = displayText[i];
                    if (newChar == ' ' || newChar == '\r' || newChar == '\n') {
                        AddLetter(newChar);
                        i++;
                    }
                    else {
                        continueAddLetter = false;
                    }
                }
            }
            else {
                AddLetter('.');
                if (ellipsisCount < 3) {
                    ellipsisCount++;
                }
                else {
                    SoundManager.PlayEvent(unmuteTelemetry);
                    ellipsisCount = 0;
                    i++;
                }
            }

            if (displayText[i] == '…' && ellipsisCount == 0) {
                SoundManager.PlayEvent(muteTelemetry);
                ellipsisCount++;
            }

            float nextLetterDelay = displayBaseSpeed;
            if ((ellipsisCount > 0 && ellipsisCount <= 3) || displayText[i - 1] == '…') {
                nextLetterDelay = 0.5f;
            }
        
            yield return new WaitForSeconds(nextLetterDelay);
            //yield return null;
        }
        if (OnEndingDisplay != null) {
            OnEndingDisplay();
        }
        SoundManager.PlayEvent(stopTelemetry);
    }

    public void AddLetter(char newChar) {
        text.text += newChar;
    }

    public void ChangeLetter(int index, char newChar) {
        StringBuilder sb = new StringBuilder(text.text);
        sb[index] = newChar;
        text.text = sb.ToString();
    }

    public IEnumerator EraseTextFromBegin() {
        int i = 0; 
        while (i < text.text.Length) {
            ChangeLetter(i++, ' ');
            yield return new WaitForSeconds(eraseBaseSpeed);
        }
    }
}
