﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System.Collections;

public class IndicationTextUI : MonoBehaviour {
    
    public TextMeshProUGUI text;
    public Image panel;
    public Button button;

    public float displayTime = 0;

    [Header("Display/Erase mode")]
    public bool displayLetterByLetter;
    public bool eraseLetterByLetter;
    public bool eraseWithFade;
    public float fadeSpeed;
    public bool removeOnFadeOut;

    [Header("Scanning Activation")]
    public bool deactivateScanningOnBegin;
    public bool activateScanningOnEnd;

    [Header("Animation")]
    public Animator animToPlay;
    public string stateBegin;
    public string stateEnd;

    public AK.Wwise.Event textAppear;
    public AK.Wwise.Event textDisappear;
    public AK.Wwise.Event okButton;

    string textComplete;
    string[] textsSplit;

    DisplayTextLetterByLetter displayTextLetterByLetter;
    int currentText;

    public delegate void EndingDisplay();
    public event EndingDisplay OnPanelRemoved;

    float panelAlpha;

    float progressFade;

    private void Awake() {
        text.text = "";
        displayTextLetterByLetter = text.GetComponent<DisplayTextLetterByLetter>();
        if (animToPlay) {
            animToPlay.GetComponent<Image>().enabled = false;
        }
        fadeSpeed = 1 / fadeSpeed;
        if (button) {
            button.image.color = new Color(button.image.color.r, button.image.color.g, button.image.color.b, 0);
        }
        if (panel) {
            panelAlpha = panel.color.a;
            panel.color = new Color(panel.color.r, panel.color.g, panel.color.b, 0);
        }
        if (animToPlay) {
            animToPlay.GetComponent<Image>().enabled = false;

        }
    }

    public void DisplayNewText(string text) {
        InitializeNewText(text);
    }

    public void DisplayNewText(TextAsset textAsset) {
        InitializeNewText(System.Text.Encoding.Default.GetString(textAsset.bytes));
    }

    public void InitializeNewText(string newText) {

        textComplete = newText;
        textsSplit = textComplete.Split('\\');
        currentText = 0;
        text.text = "";
        text.color = new Color(text.color.r, text.color.g, text.color.b, 1);
        if (button) {
            button.image.color = new Color(button.image.color.r, button.image.color.g, button.image.color.b, 0);
        }
        if (panel) {
            panel.color = new Color(panel.color.r, panel.color.g, panel.color.b, 0);
        }
        StartCoroutine(FadeInUI());
        if (!displayTextLetterByLetter) {
            displayTextLetterByLetter = text.GetComponent<DisplayTextLetterByLetter>();
        }
        if (displayLetterByLetter) {
            displayTextLetterByLetter.StartDisplayText(textsSplit[currentText++]);

        }
        else if (!displayLetterByLetter) {
            text.text = newText;
        }
        if (displayTime > 0) {
            StartCoroutine(WaitForErase());
        }
        if (animToPlay) {
            animToPlay.GetComponent<Image>().enabled = true;
            animToPlay.Play(stateBegin);
        }
        SoundManager.PlayEvent(textAppear);
    }

    public IEnumerator WaitForErase() {
        yield return new WaitForSeconds(displayTime);
        EraseText();
    }

    public void EraseText() {
        if (!eraseLetterByLetter && !eraseWithFade) {
            text.text = "";
        }
        else if (eraseLetterByLetter){
            displayTextLetterByLetter.EraseTextLetterByLetter();
        }
        else if (eraseWithFade) {
            StartCoroutine(EraseWithFade());
        }
        if (animToPlay) {
            animToPlay.Play(stateEnd);
            StartCoroutine(DisabledImageAfterAnim());
        }
        SoundManager.PlayEvent(textDisappear);
    }

    

    public void NextText() {
        SoundManager.PlayEvent(displayTextLetterByLetter.stopTelemetry);
        SoundManager.PlayEvent(okButton);
        if (currentText < textsSplit.Length && displayTextLetterByLetter.AllTextDisplayed) {
            displayTextLetterByLetter.StartDisplayText(textsSplit[currentText++]);
        }
        else if (currentText <= textsSplit.Length && !displayTextLetterByLetter.AllTextDisplayed) {
            displayTextLetterByLetter.DisplayAllText();
            if (currentText == textsSplit.Length) {
                currentText++;
            }
        }
        else {
            if (eraseWithFade) {
                EraseText();
            }
            else {
                text.text = "";
                
            }
            
        }
    }

    public IEnumerator EraseWithFade() {
        while (text.color.a > 0) {
            text.color = new Color(text.color.r, text.color.g, text.color.b, text.color.a - (fadeSpeed * Time.deltaTime));
            if (button) {
                button.image.color = new Color(button.image.color.r, button.image.color.g, button.image.color.b, button.image.color.a - (fadeSpeed * Time.deltaTime));
            }
            if (panel) {
                panel.color = new Color(panel.color.r, panel.color.g, panel.color.b, panel.color.a - (fadeSpeed * Time.deltaTime));
            }
            yield return null;
        }
        if (panel) {
            panel.gameObject.SetActive(false);
        }
        if (OnPanelRemoved != null) {
            OnPanelRemoved();
        }
        GameManager.Instance.State = GameManager.GameState.Playing;
        if (removeOnFadeOut) {
            panel.gameObject.SetActive(false);
        }
    }

    public IEnumerator FadeInUI() {
        progressFade = 0;
        while (progressFade < 1) {
            progressFade += (fadeSpeed * Time.deltaTime);
            if (button) {
                button.image.color = new Color(button.image.color.r, button.image.color.g, button.image.color.b, progressFade);
            }
            if (panel && panel.color.a < panelAlpha) {
                panel.color = new Color(panel.color.r, panel.color.g, panel.color.b, progressFade);
            }
            yield return null;
        }
    }

    public IEnumerator DisabledImageAfterAnim() {
        
        float delay = GetClipLength(animToPlay, stateEnd);
        yield return new WaitForSeconds(delay);

        animToPlay.GetComponent<Image>().enabled = false;
    }

    public float GetClipLength(Animator animator, string stateName) {
    AnimationClip[] clips = animator.runtimeAnimatorController.animationClips;
    float delay = 0;
    foreach (AnimationClip clip in clips) {
        if (clip.name == stateName) {
            delay = clip.length;
        }
    }
    return delay;
}
}
