﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class FadeAlpha : MonoBehaviour {

    public AnimationCurve alphaTransition;

    Renderer rendererToFade;
    Image imageToFade;
    Color color;
    float timer = 0f;
    bool fade;

    private void Awake() {
        imageToFade = GetComponent<Image>();
        if (imageToFade) {
            color = imageToFade.color;
        }
        else {
            rendererToFade = GetComponent<Renderer>(); // do this in awake, it has an impact on performances in Update
            color = rendererToFade.material.color;
        }
    }

    private void Update() {
        if (fade) {
            timer += Time.deltaTime;
            color.a = alphaTransition.Evaluate(timer);
            if (imageToFade) {
                imageToFade.color = color;
            }
            else {
                rendererToFade.material.color = color;
            }
        }
    }

    public void Fade() {
        fade = true;
    }

    
}
