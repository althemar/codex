﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class HintManager : MonoBehaviour {

    public static HintManager Instance;
    
    public Button hintButton;
    public Image hintBubble;
    public Text hintText;

    bool hasHint;
    bool hintActivated;



    Transform originalTransform;


    HintSO currentHintSO;

    Coroutine co;

    void SetHint() {

    }

  
    private void Awake() {
        

        if (Instance == null) {
            Instance = this;
        }
        else {
            Destroy(gameObject);
        }
        hintButton.interactable = false;
        hintBubble.enabled = false;
        hintText.enabled = false;
    }

    public void SwitchHintState() {
        if (hasHint) {
            hintActivated = !hintActivated;
            hintBubble.enabled = hintActivated;
            hintText.enabled = hintActivated;
        }
    }

    public void SetHint(HintSO hintSO) {
        if (hintSO == currentHintSO) {
            return;
        }

        if (hintActivated) {
            SwitchHintState();
        }
        hasHint = true;
        hintText.text = hintSO.text;
        currentHintSO = hintSO;
        if (co != null) {
            StopCoroutine(co);
        }
        //co = StartCoroutine("WaitBeforeHint");
    }

    public void ValidateHint() {
        if (hintActivated) {
            SwitchHintState();
        }
        hintText.text = "";
        hintButton.interactable = false;
        hasHint = false;
    }

    public IEnumerator WaitBeforeHint() {
        hintButton.interactable = false;
        yield return new WaitForSeconds(currentHintSO.timeBeforeSpawn);
        hintButton.interactable = true;
    }

}
