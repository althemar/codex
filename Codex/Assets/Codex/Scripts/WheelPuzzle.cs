﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

/*
 * WheelPuzzle
 * A wheel puzzle is a puzzle with rings inside. 
 * The player must find the good rotations of the rings to solve it. 
 * The rings can be rotated freely, but their are snapping when released
 */

public class WheelPuzzle : MonoBehaviour
{
    /*
     * Members
     */

    public float SymbolsCount;  // The number of symbols on each ring
    public UnityEvent OnSolved; // Events to launch when wheel puzzle is solved 
    public float burningSpeed = 2f;

    public WheelPuzzleState wheelState = WheelPuzzleState.Locked;

    [Header("Wwise")]
    public AK.Wwise.Event validated = null;
    public List<AK.Wwise.Event> ringSelected;
    public AK.Wwise.Event ringReleased;

    List<Ring> rings;               // The list of rings
    float previousAngleIndex = -1;  // The previous angle when the puzzle is initializing

    float symbolSize;               // The angle for each symbol on a ring
    List<float> anglesList;         // The list of angles for snapping the symbols 

    List<Renderer> objectsToBurn;
    float burningProgress = 1f;

    AugmentedObject augmented;

    const string ringToBurnTag = "RendererToBurn";
    const string shaderProgressVariable = "_Progress";
    const string textHelpPath = "Texts/wheelPuzzleHelp";

    /*
     * Properties
     */

    public WheelPuzzleState WheelState
    {
        get { return wheelState; }
        set { wheelState = value; }
    }

    public float SymbolSize
    {
        get { return symbolSize; }
    }

    public List<float> AnglesList
    {
        get { return anglesList; }
    }

    public float BurningProgress
    {
        get { return burningProgress; }
    }

    public AugmentedObject Augmented
    {
        get { return augmented; }
    }

    /*
     * Methods
     */

    void Awake()
    {
        augmented = GetComponent<AugmentedObject>();

        FillSymbolsOrientation();

        InitializeRingsList();

        RotateRingsRandomly();

        burningSpeed = 1f / burningSpeed;
        
    }

    // Fill the list of angles;
    private void FillSymbolsOrientation()
    {
        symbolSize = 360 / SymbolsCount;
        float angle = symbolSize / 2;
        anglesList = new List<float>();
        for (int i = 0; i < SymbolsCount; i++) {
            anglesList.Add(angle);
            angle += symbolSize;
        }
    }

    // Initialize the list of rings
    private void InitializeRingsList() {
        rings = new List<Ring>();
        objectsToBurn = new List<Renderer>();

        for (int i = 0; i < transform.childCount; i++) {
            if (transform.GetChild(i).CompareTag(ringToBurnTag)) { 
                objectsToBurn.Add(transform.GetChild(i).GetComponent<Renderer>());
            }

            Ring ring = transform.GetChild(i).GetComponent<Ring>();
            if (ring) {
                rings.Add(ring);
            }
        }
    }

    // Rotate the rings randomly
    private void RotateRingsRandomly() {
        for (int i = 0; i < rings.Count; i++) {
            RotateRingRandomly(i);
        }
    }

    private void RotateRingRandomly(int i) {
        int angleIndex = Random.Range(0, anglesList.Count);
        while (angleIndex == previousAngleIndex) {
            angleIndex = Random.Range(0, anglesList.Count);
        }
        previousAngleIndex = angleIndex;
        float newSymbolRotation = anglesList[angleIndex] - symbolSize / 2;
        rings[i].transform.Rotate(new Vector3(0, 0, newSymbolRotation));
    }


    // Check if all the rings are aligned
    public void CheckIfSolved()
    {
        if (wheelState == WheelPuzzleState.Solved)
            return;
        for (int i = 0; i < transform.childCount; i++)
        {
            Ring ring = transform.GetChild(i).GetComponent<Ring>();
            if (ring)
            {
                float originalRotation = 0;
                float currentRotation = ring.transform.localEulerAngles.y;
                if (currentRotation > 180) {
                    currentRotation = 360 - currentRotation;
                }

                float decalage = 0;/*
                if ( ring.OriginalRotation <= 180) {
                    decalage = ring.OriginalRotation;
                }*/
                if (ring.transform.localEulerAngles.y <= 180 && ring.OriginalRotation <= 180){
                    if (ring.transform.localEulerAngles.y > ring.OriginalRotation) {
                        currentRotation -= ring.OriginalRotation;
                    }
                    else {
                        currentRotation = ring.OriginalRotation - currentRotation;
                    }
                }
                else if (ring.transform.localEulerAngles.y <= 180 && ring.OriginalRotation > 180) {
                    if (ring.transform.localEulerAngles.y + (360 - ring.OriginalRotation) <= 180) {
                        currentRotation += (360 - ring.OriginalRotation);
                    }
                    else {
                        currentRotation = ring.OriginalRotation - currentRotation;
                    }
                }
                else if (ring.transform.localEulerAngles.y > 180 && ring.OriginalRotation <= 180) {
                    if (currentRotation + ring.OriginalRotation <= 180) {
                        currentRotation += ring.OriginalRotation;
                    }
                    else {
                        currentRotation = ring.OriginalRotation - currentRotation;
                    }
                }
                else if (ring.transform.localEulerAngles.y > 180 && ring.OriginalRotation > 180) {
                    if (ring.transform.localEulerAngles.y > ring.OriginalRotation) {
                        currentRotation = (360 - ring.OriginalRotation) - currentRotation;
                    }
                    else {
                        currentRotation -= (360 - ring.OriginalRotation);
                    }
                }

                Debug.Log(currentRotation);
                /*
                if (ring.transform.localEulerAngles.y > ring.OriginalRotation) {
                    currentRotation = ring.transform.localEulerAngles.y - ring.OriginalRotation;
                }
                else {
                    currentRotation = ring.transform.localEulerAngles.y + ring.OriginalRotation * 2;
                }*/
                float diff = AngleDifference(originalRotation, currentRotation);
                if (diff >= symbolSize / 2)
                {
                    return;
                }
            }
        }
        wheelState = WheelPuzzleState.Solved;
        OnSolved.Invoke();
        StartCoroutine(Burn());
        SoundManager.PlayEvent(validated);

    }

    IEnumerator Burn() {
        GetComponent<LockableAugmented>().Validate();

        while (burningProgress >= 0) {
            burningProgress -= burningSpeed * Time.deltaTime;
            for (int i = 0; i < objectsToBurn.Count; i++) {
                objectsToBurn[i].material.SetFloat(shaderProgressVariable, burningProgress);
            }
            yield return null;
        }
        Destroy(gameObject);
    }
    

    public void OnLostTrack() {
        if (wheelState == WheelPuzzleState.Solved) {
            Destroy(gameObject);
        }
    }

    // Return the angle between to orientations
    public float AngleDifference(float original, float angleToCompare)
    {
        float difference;
        if (angleToCompare <= original + 180)
        {
            difference = angleToCompare - original;
        }
        else
        {
            float newAngle = angleToCompare - 180;
            difference = original + 180 - newAngle;
        }
        return difference;
    }

    public void Unlock() {
        wheelState = WheelPuzzleState.Unlocked;
    }

    public int GetRingIndex(Ring ring) {
        for (int i = 0; i < rings.Count; i++) {
            if (rings[i] == ring) {
                return i;
            }
        }
        return -1;
    }
}
