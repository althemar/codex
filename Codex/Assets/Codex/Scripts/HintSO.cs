﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Hint", menuName = "Codex/Hint")]
public class HintSO : ScriptableObject
{
    public string text;
    public float timeBeforeSpawn;
}
