﻿using UnityEngine;

public class ConstellationSymbol : MonoBehaviour {

    bool validated = false;

    public Constellation constellation;

    public Renderer symbolRenderer;
    public Material unvalidatedMat;
    public Material validatedMat;

    public ParticleSystem neutralPS;
    public ParticleSystem activatedPS;

    public Color neutralColor;
    public Color rightSymbolColor;
    public Color wrongSymbolColor;

    public float speedColorChange = 2f;

    bool colorChanging;
    float progressColor;
    Color beginColor;
    Color targetColor;

    Burn burn;



    private void Start() {
        speedColorChange = 1f / speedColorChange;
        burn = symbolRenderer.GetComponent<Burn>();
    }

    private void Update() {
        if (colorChanging) {
            progressColor += speedColorChange * Time.deltaTime;
            Color currentColor = Color.Lerp(beginColor, targetColor, progressColor);
            var neutralMain = neutralPS.main;
            neutralMain.startColor = currentColor;

            if (progressColor >= 1) {
                colorChanging = false;
            }
        }
    }



    public void TryUnlock() {
        if (!constellation.Validated && constellation.symbolsToScan[constellation.IndexToUnlock] == this) {
            int index = constellation.SymbolIndex(this);
            if (index < constellation.symbolsToScan.Count - 1) {
                SoundManager.PlayEvent(constellation.rightSymbol);
            }
            else {
                SoundManager.PlayEvent(constellation.allSymbolScanned);
            }
            validated = true;
            symbolRenderer.material = validatedMat;
            ActivateParticleSystem(rightSymbolColor, rightSymbolColor);
            constellation.IndexToUnlock++;
            constellation.CheckIfValidated();
        }
        else if (!constellation.Validated && validated == false) {
            constellation.ResetSymbols();
            ActivateParticleSystem(neutralColor, wrongSymbolColor);
        }
    }

    public void Unvalidate() {
        if (validated) {
            validated = false;
            ActivateParticleSystem(neutralColor, wrongSymbolColor);
            symbolRenderer.material = unvalidatedMat;
        }
    }

 

    public void ActivateParticleSystem(Color neutral, Color explosion) {
        progressColor = 0;
        colorChanging = true;
        var neutralMain = neutralPS.main;
        beginColor = neutralMain.startColor.color;
        targetColor = neutral;
        var activateMain = activatedPS.main;
        activateMain.startColor = explosion;
        activatedPS.Play();
    }

    public void Stop() {
        if (burn) {
            burn.StartBurn(false);
            burn.OnBurningComplete += DeactivateSymbol;
        }
        else {
            symbolRenderer.gameObject.SetActive(false);
        }
        neutralPS.Stop();
        activatedPS.Stop();
    }

    public void DeactivateSymbol() {
        symbolRenderer.gameObject.SetActive(false);
    }
}
