﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnglePlane : MonoBehaviour {

    Ray ray;
    RaycastHit hit;
    AnamorphosisPlane planeHit;

    Camera cam;
     
    // Use this for initialization
    void Start () {
        cam = Camera.main;
	}
	
	// Update is called once per frame
	void Update () {

        Vector3 delta = cam.transform.position - transform.position;
        Quaternion look = Quaternion.LookRotation(delta);
        float vertical = look.eulerAngles.x;
        float horizontal = look.eulerAngles.y;
        Debug.Log(vertical + "   " + horizontal);

        //Debug.Log(AngleInPlane(cam.transform, transform.position, Vector3.up));
        //Debug.Log(AngleInPlane(cam.transform, transform.position, Vector3.right));

        //ray = cam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
        //if (Physics.Raycast(ray, out hit)) {
        //    Vector3 incomingVec = hit.point - cam.transform.position;
        //    float angle = Mathf.Atan2(incomingVec.y, incomingVec.z) * Mathf.Rad2Deg;
        //    AkSoundEngine.SetRTPCValue("interactive", angle * -1);
        //}
    }

    public float AngleInPlane(Transform from, Vector3 to, Vector3 planeNormal) {
        Vector3 dir = to - from.position;

        Vector3 p1 = Project(dir, planeNormal);
        Vector3 p2 = Project(from.forward, planeNormal);

        return Vector3.Angle(p1, p2);
    }

    public Vector3 Project(Vector3 v, Vector3 onto) {
        return v - (Vector3.Dot(v, onto) / Vector3.Dot(onto, onto)) * onto;
    }
}
