﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Burn : MonoBehaviour {

    public string variableDeMortDuShader;



    Renderer rendererToBurn;

    float burningProgress;
    float burningTarget;

    bool burningComplete;

    AugmentedObject augmented;

    public UnityEvent onAppearComplete;
    public UnityEvent onDisappearComplete;

    public bool burnOnScan = true;

    public delegate void BurnComplete();
    public event BurnComplete OnBurningComplete;

    public AugmentedObject Augmented
    {
        get { return augmented; }
        set { augmented = value; }
    }
    
	void Start () {
        rendererToBurn = GetComponent<Renderer>();
	}

    public void StartBurn(bool appear) {
        if (!rendererToBurn || !burnOnScan) {
            rendererToBurn = GetComponent<Renderer>();
        }
        if (appear) {
            burningProgress = 0;
            burningTarget = 1;

        }
        else {
            burningProgress = 1;
            burningTarget = 0;
        }
        rendererToBurn.material.SetFloat(variableDeMortDuShader, burningProgress);
        burningComplete = false;
        StopCoroutine("BurnCoroutine");

        StartCoroutine("BurnCoroutine");
    }

    IEnumerator BurnCoroutine() {
        if (burningTarget == 1) {
            while (burningProgress <= 1) {
                burningProgress += augmented.burningDuration * Time.deltaTime;
                rendererToBurn.material.SetFloat(variableDeMortDuShader, burningProgress);
                yield return null;
            }
            onAppearComplete.Invoke();
        }
        else {
            while (burningProgress >= 0) {
                burningProgress -= augmented.burningDuration * Time.deltaTime;
                rendererToBurn.material.SetFloat(variableDeMortDuShader, burningProgress);
                yield return null;
            }
            onDisappearComplete.Invoke();
        }
        burningComplete = true;
        augmented.BurningComplete = true;
        if (OnBurningComplete != null) {
            OnBurningComplete();
        }
    }
}
