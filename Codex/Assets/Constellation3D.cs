﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class Constellation3D : MonoBehaviour
{

    public List<ConstellationSymbol3D> symbolsToScan;

    public UnityEvent onAllSymbolsScanned;

    public bool activated;

    public Material symbolActivated;
    public Material symbolUnactivated;

    [Header("Wwise")]
    public AK.Wwise.Event notActivated;
    public AK.Wwise.Event interactable;
    public AK.Wwise.Event rightSymbol;
    public AK.Wwise.Event wrongSymbol;
    public AK.Wwise.Event allSymbolScanned;

    const string textHelpPath = "Texts/symbolHelp";

    int indexToUnlock = 0;

    bool validated;


    public int IndexToUnlock
    {
        get { return indexToUnlock; }
        set { indexToUnlock = value; }
    }

    public bool Validated
    {
        get { return validated; }
        set { validated = value; }
    }

    public void ResetSymbols() {
        UIManager.Instance.NotifInvalidCombination();

        SoundManager.PlayEvent(wrongSymbol);
        indexToUnlock = 0;
        for (int i = 0; i < symbolsToScan.Count; i++) {
            symbolsToScan[i].Unvalidate();

        }
    }

    public void CheckIfValidated() {
        if (indexToUnlock == symbolsToScan.Count) {
            validated = true;
            onAllSymbolsScanned.Invoke();
        }
    }

    public void OnTrack() {
        if (!activated) {
            SoundManager.PlayEvent(notActivated);
        }
        else if (!validated) {
            SoundManager.PlayEvent(interactable);
        }

        

    }


    public int SymbolIndex(ConstellationSymbol3D symbol) {
        for (int i = 0; i < symbolsToScan.Count; i++) {
            if (symbolsToScan[i] == symbol) {
                return i;
            }
        }
        return -1;
    }

    public void Activate() {
        activated = true;
    }
}
