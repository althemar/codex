﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Rendering.PostProcessing;
using TMPro;

public class EndingPanel : MonoBehaviour {
    

    public enum EndingState
    {
        playing,
        algorithm,
        shutdown,
        codex,
        end
    }

    public Image algorithm;
    public Image shutdown;
    public Image backgroundPanel;
    public Image codex;

    public string algorithmState;
    public string shutdownState;
    public string codexState;

    public AK.Wwise.Event algorithmGlitch;
    public AK.Wwise.Event shutdownGlitch;
    public AK.Wwise.Event codexGlitch;
    public AK.Wwise.State endingState;

    Animator algorithmAnimator;
    Animator shutdownAnimator;
    Animator codexAnimator;

    EndingState state = EndingState.playing;

    private void Awake() {
        backgroundPanel.enabled = false;
        codex.enabled = false;
        algorithm.enabled = false;
        shutdown.enabled = false;

        algorithmAnimator = algorithm.GetComponent<Animator>();
        shutdownAnimator = shutdown.GetComponent<Animator>();
        codexAnimator = codex.GetComponent<Animator>();

        PostProcessManager.Instance.OnEndingChromaticAberration += StartEnding;
    }

    

	void Update () {
        /*
       if (state == EndingState.algorithm) {
            if (!algorithmAnimator.GetCurrentAnimatorStateInfo(0).IsName(algorithmState)) {
                state = EndingState.shutdown;
                shutdown.enabled = true;

                shutdownAnimator.Play(shutdownState);
                algorithm.enabled = false;
            }
        }
        if (state == EndingState.shutdown) {
            if (!shutdownAnimator.GetCurrentAnimatorStateInfo(0).IsName(shutdownState)) {
                state = EndingState.codex;
                codexAnimator.Play(codexState);
                codex.enabled = true;
                backgroundPanel.enabled = true;
                shutdown.enabled = false;

            }

        }
        if (state == EndingState.codex) {
            if (!codexAnimator.GetCurrentAnimatorStateInfo(0).IsName(codexState)) {
                state = EndingState.playing;
            }
        }*/
    }

    public void StartEnding()
    {
        state = EndingState.shutdown;
        algorithm.enabled = true;
        StartCoroutine(PlayAnims());
        //shutdownAnimator.Play(shutdownState);
        //shutdown.enabled = true;


    }

    public IEnumerator PlayAnims() {

        algorithmAnimator.Play(algorithmState);
        SoundManager.SetState(endingState);
        SoundManager.PlayEvent(algorithmGlitch);

        float delay = GetClipLength(algorithmAnimator, algorithmState);
        yield return new WaitForSeconds(delay);

        algorithm.enabled = false;
        shutdown.enabled = true;

        shutdownAnimator.Play(shutdownState);
        SoundManager.PlayEvent(shutdownGlitch);
        delay = GetClipLength(shutdownAnimator, shutdownState);
        yield return new WaitForSeconds(delay);

        shutdown.enabled = false;
        codex.enabled = true;
        backgroundPanel.enabled = true;

        codexAnimator.Play(codexState);
        SoundManager.PlayEvent(codexGlitch);
        delay = GetClipLength(codexAnimator, codexState);
        yield return new WaitForSeconds(delay - 0.2f);

        codex.enabled = false;


    }

    public float GetClipLength(Animator animator, string stateName) {
        AnimationClip[] clips = animator.runtimeAnimatorController.animationClips;
        float delay = 0;
        foreach (AnimationClip clip in clips) {
            if (clip.name == stateName) {
                delay = clip.length;
            }
        }
        return delay;
    }




}
